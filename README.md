Rquirements:  
------------
Numpy  
Python 3.5 
Tensroflow   
OpenCV  


Steps to use this code:  
----------------------

1) Go to utils/ and run:   
   $ `python config.py`   
   this downloads the darknet weight files. Also, fuses batchnorm layers and creates TensorFlow Ckpt files.  

2) To run image inference:  
   $ `python inference.py` ,       to run TinyYolo model  
   $ `python inference.py --image= [image path]`  
   $ `python infernce.py --v2` ,   to run YoloV2 model  
   $ `NUM_INTER_THREADS=2 NUM_INTRA_THREADS=8 python inference.py  --par` ,    to run parallel TensorFlow session(Inter/Intra op threads), if it is supported in your system.  
  
3) To run Webcam inference:  
   $ `python multi-tracking.py`    
  

  
